package gameoflife;

public class Frame {

    public final static String ALIVE = "X";
    public final static String DEAD = "-";

    public Frame(int width, int height) {
        // konstruktor
    }

    @Override
    public String toString() {

        // tagastab stringi mis kujutab kaadri sisu

        return null;
    }

    @Override
    public boolean equals(Object obj) {

        // Võrdleb kahte kaadrit.

        // Kaadrid on võrdsed, kui need on sama suurusega
        // ja vastavatel positsioonidel on sama olekuga (elus/surnud) rakud.

        // Tõene, kui kaadrid on võrdsed.

        return false;
    }

    public Integer getNeighbourCount(int x, int y) {

        // Tagastab raku kordinaatidega X ja Y naabrite arvu.
        // Võimalikud väärtused on 0-8

        return null;
    }

    public boolean isAlive(int x, int y) {

        // Tõene, kui rakk kordinaatidega X ja Y on elus.

        return false;
    }

    public void markAlive(int x, int y) {

        // Märgib, raku kordinaatidega X ja Y elus olevaks.

    }

    public Frame nextFrame() {

        // Tagastab järgmise kaadri.

        // Looge uus, eelmisega sama suurusega, puhas kaader
        // Arvutage vana kaadri pealt iga raku järgmine seisund
        // (arvestades naabrite hulka) ja märkige see seis uude kaadrisse.
        // Tagastage uus kaader.

        return null;
    }

}
